import { Button, Col, Container, Row } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import Carousel from 'react-bootstrap/Carousel';

import logo2 from '../icons/logo2.png';
import img1 from '../images/img1.JPG';
import img2 from '../images/img2.jpg';
import img3 from '../images/img3.jpg';

export default function Banner({ isHomePage }) {
  return (
    <Container className="d-flex flex-column justify-content-center align-items-center vh-80">
      {isHomePage ? (
        <>
          <img src={logo2} alt="Logo" className="mb-4" style={{ maxWidth: '100%', height: 'auto' }} />
          <h1 className="text-center">We make it ez</h1>
          <h3></h3>
          <Button variant="primary" as={NavLink} to="/products">
            Browse Catalog
          </Button>
        </>
      ) : (
        <>
          <h1 className="text-center">Page Not Found</h1>
          <p className="text-center">
            Go back to the <Link to="/">Homepage</Link>.
          </p>
        </>
      )}

      {isHomePage && (
  <div className="carousel-container col-lg-6 mt-5 text-center">
    <Carousel>
      <Carousel.Item>
        <img className="carousel-image img-fluid mx-auto" src={img1} alt="Image 1" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="carousel-image img-fluid mx-auto" src={img2} alt="Image 2" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="carousel-image img-fluid mx-auto" src={img3} alt="Image 3" />
      </Carousel.Item>
    </Carousel>
  </div>
)}
    </Container>
  );
}
