import {useContext} from 'react';
import { Form, Button,Card } from 'react-bootstrap';

import {useState,useEffect} from 'react';
import {UserProvider} from '../UserContext';

import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function ProductCard({product, isAdminPage}){


const {_id,name,description,price,imageUrl,isActive,addedBy} = product;
const [disable,setDisable]=useState(null)
const {user} = useContext(UserContext);
const [isLoading, setIsLoading] = useState(true);
const [isEmpty, setIsEmpty] = useState(false); 
console.log(`items ${product}`)

 useEffect(() => {
    setIsEmpty(product === null||product===undefined);
  }, [product]);


function disableProduct(productId) {

      	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      	  method: "PATCH",
      	  headers: {
      	    "Content-Type": "application/json",
      	    Authorization : `Bearer ${ localStorage.getItem('token') }`
      	  }
      	})

        Swal.fire({
          icon: 'success',
          title: 'Product successfully archived the product.',
          text: 'You have successfully archived the product.'
        })

}

function enableProduct(productId) {

      	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
      	  method: "PATCH",
      	  headers: {
      	    "Content-Type": "application/json",
      	    Authorization : `Bearer ${ localStorage.getItem('token') }`
      	  }
      	})

        Swal.fire({
          icon: 'success',
          title: 'Product successfully activated the product.',
          text: 'You have successfully activated the product.'
        })

}
  if (isEmpty) {
    <div  style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '200px' }}>Loading products...</div>
  }

	return (
		<>

		{isAdminPage ? (

			<Card style={{ backgroundColor: 'darkgrey' }} className="my-3 container-fluid d-xl-flex justify-content-center col-xl-6 mb-5 mb-xl-0  ml-xl-auto">
			<Card.Body>

						<Card.Title>
							<h3>{name}</h3>
						</Card.Title>
						<Card>
						{imageUrl && <Card.Img variant="top" src={imageUrl} alt={name} />}
						<Card.Body>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text style={{ color: 'black' }}>{description}</Card.Text>
						<Card.Subtitle>Active:</Card.Subtitle>
						<Card.Text style={{ color: 'black' }}>{isActive.toString()}</Card.Text>
						<Card.Subtitle>Added By:</Card.Subtitle>	
						<Card.Text style={{ color: 'black' }}>{addedBy}</Card.Text>	
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text style={{ color: 'black' }}>Php {price}</Card.Text>


						<Link className="btn btn-primary" to={`/products/${_id}`}>Update</Link><br />
						{isActive ? (
  <Button variant="danger" onClick={() => disableProduct(_id)} id="submitBtn" className="mt-2 float-right">Disable</Button>
) : (
  <Button variant="success" onClick={() => enableProduct(_id)} id="submitBtn" className="mt-2 float-right">Enable</Button>
)}
					</Card.Body>
				</Card>
					</Card.Body>
				</Card>
				):(

				<Card style={{ backgroundColor: 'darkgrey' }} className="my-3 container-fluid d-xl-flex justify-content-center col-xl-6 mb-5 mb-xl-0  ml-xl-auto">
				
				<Card.Body>

						<Card.Title>{name}</Card.Title><br/>
						{imageUrl && <Card.Img variant="top" src={imageUrl} alt={name} />}
						<Card>
						<Card.Body>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text style={{ color: 'black' }}>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text style={{ color: 'black' }}>Php {price}</Card.Text><br/>
	
						<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
					</Card.Body>
				</Card>



				)}
			</>

		)
		   


}



