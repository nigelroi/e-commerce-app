import { useContext } from 'react';
import { Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function OrderCard({ orders}) {
  const { products, purchasedOn, totalAmount, _id } = orders;
  const purchasedDate = new Date(purchasedOn).toLocaleDateString();

  const { user } = useContext(UserContext);

  return (
  
        <>
        <Card style={{ backgroundColor: 'darkgrey' }} className="my-3">
          <Card.Body>
          <Card>
            <Card.Body>
              <Card.Subtitle>Products:</Card.Subtitle>
              {products.map((product) => (
                <Card.Text key={product._id} style={{ color: 'black' }}>
                  Product ID: {product.productId}<br />
                  Product Name: {product.productName}<br />
                  Quantity: {product.quantity}
                </Card.Text>
                ))}
              <Card.Subtitle>Purchased date:</Card.Subtitle>
              <Card.Text style={{ color: 'black' }}>{purchasedDate}</Card.Text>
              <Card.Subtitle>Total:</Card.Subtitle>
              <Card.Text style={{ color: 'black' }}>{totalAmount}</Card.Text>
              <Card.Subtitle>Order Id:</Card.Subtitle>
              <Card.Text style={{ color: 'black' }}>{_id}</Card.Text>
            </Card.Body>
          </Card>
            </Card.Body>
          </Card>
    </>
  );
}