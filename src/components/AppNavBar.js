import {useContext} from 'react';

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link,NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

import logo from '../icons/logo.png'
import logo2 from '../icons/logo2.png'
import logo3 from '../icons/logo3.png'
export default function AppNavBar(){
  const isActiveLink = (path) => {
    return location.pathname === path;
  };
  const {user} = useContext(UserContext);
  console.log(user)
  return(
    <>
    <Navbar
      expand="lg"
      variant="dark"
      id="Navbar"
      className="Navbar-links"
      style={{ position: 'fixed', top: 0, left: 0, right: 0, zIndex: 100, backgroundColor: 'rgb(35, 47, 62)' }}
    >
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
          <img src={logo3} alt="Logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" activeclassname="active">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products" activeclassname="active">Products</Nav.Link> 
            {user.id ? (
              <>
                <Nav.Link as={NavLink} to="/profile" activeclassname="active">
                  Profile
                </Nav.Link>
                {user.isAdmin ? (
                  <>
                    <Nav.Link as={NavLink} to="/admin" activeclassname="active">
                      Admin Dashboard
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/logout" activeclassname="active">
                      Logout
                    </Nav.Link>
                  </>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/logout" activeclassname="active">
                      Logout
                    </Nav.Link>
                  </>
                )}
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" activeclassname="active">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" activeclassname="active">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
<div style={{ paddingTop: '100px' }}>
 </div>
 </>
  );
  
}
