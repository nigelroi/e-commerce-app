import { useContext, useState } from 'react';
import { Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AllOrdersCard({ allOrders, isOrders }) {
  const { productName, productId, userOrders } = allOrders;
  const { user } = useContext(UserContext);

  return (
    <Card style={{ backgroundColor: 'darkgrey' }} className="my-3">
      <Card.Body>
        <Card.Title>Product Name: {productName}</Card.Title>
        <Card.Subtitle>Product ID: {productId}</Card.Subtitle>
        
        {userOrders.map((order) => (
          <Card className="my-2" key={order._id}>
            <Card.Body>
              <Card.Text style={{ color: 'black' }}>
                Email: {order.userEmail}<br />
                User ID: {order.userId}<br />
                Quantity: {order.quantity}<br />
                Order ID: {order._id}
              </Card.Text>
            </Card.Body>
          </Card>
        ))}
      </Card.Body>
    </Card>
  );
}
