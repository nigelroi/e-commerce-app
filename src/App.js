
import {useState,useEffect} from 'react'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Routes} from 'react-router-dom';

import AdminView from './pages/Admin';
import AppNavBar from './components/AppNavBar';
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Profile from './pages/Profile';
import ProductView from './pages/ProductView'
import UpdateProduct from './pages/UpdateProduct'

import './App.css';
import {UserProvider} from './UserContext';

function App(){

    const [user, setUser] = useState({ firstName: null,lastName:null, email:null,id : null, isAdmin : null });

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        if(localStorage.getItem('token')){
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);


                  setUser({
                    firstName:data.firstName,
                    lastName:data.lastName,
                    email:data.email,
                    points:data.points,
                    id: data._id,
                    isAdmin: data.isAdmin,
                    orderedProduct:data.orderedProduct
                  })
        });

}
        console.log(user);
        console.log(localStorage);
    }, []);

    return (


    <UserProvider value={{ user, setUser, unsetUser }}>

        <Router>      
            <AppNavBar />
            <Container>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/products" element={<Products />} />
                    <Route path="/admin" element={<AdminView />} />                    
                    <Route path="/products/:productId" element={<ProductView />} />
                    <Route path="/products/update/:productId" element={<UpdateProduct />} />
                    <Route path="/profile" element={<Profile />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} />       
                    <Route path="/*" element={<ErrorPage />} />     
                </Routes>
            </Container>
        </Router>
    </UserProvider>

      
  );
}

export default App;
