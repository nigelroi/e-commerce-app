import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Button, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import saleImage from '../images/sale.jpg';


export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [quantity, setQuantity] = useState('1');
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [addedBy, setaddedBy] = useState('');
  const [imageUrl, setImageUrl] = useState('');

  const order = (productId) =>
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.includes(true)) {
          Swal.fire({
            icon: 'success',
            title: 'Successful purchased',
            text: 'Item successfully checked out!',
          });
          navigate('/products');
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Something went wrong',
            text: 'Please try again.',
          });
        }
      });

function updateProduct(e) {
  e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization : `Bearer ${ localStorage.getItem('token') }`
          },
          body: JSON.stringify({
            description: description,
            price: price,
          })
        })
        Swal.fire({
          icon: 'success',
          title: 'Product successfully updated the product.',
          text: 'You have successfully updated the product.'
        });
        navigate("/admin");

}
  useEffect(() => {
    console.log(productId);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProductName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setIsActive(data.isActive);
        setaddedBy(data.addedBy);
        setImageUrl(data.imageUrl)
      });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
    <Card style={{ backgroundColor: 'darkgrey' }} className="my-3">
            <Card.Body className="text-center">
              
              {user.isAdmin ? (
                <>

                      <Card>
                      {imageUrl && <Card.Img variant="top" src={imageUrl} alt={name} />}

                        <Card.Body className="text-center">
                          <Card.Title><h1>{productName}</h1></Card.Title>
                          <Card.Subtitle>Description:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                            {description}
                          </Card.Text>

                          <Card.Subtitle>Is Active:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                           {isActive.toString()}
                          </Card.Text>
                          <Card.Subtitle>Added By:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                            {addedBy}
                          </Card.Text>
                          <Card.Subtitle>Price:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                            Php {price}
                          </Card.Text>
                        </Card.Body>
                      </Card>

                  <Form onSubmit={(e) => updateProduct(e)}>
                    <Form.Group controlId="productName">
                      <Form.Label style={{ color: 'black' }}>Product Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter the product's name"
                        value={productName}
                        onChange={(e) => setProductName(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="description">
                      <Form.Label style={{ color: 'black' }}>Description</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter product description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Form.Group controlId="price">
                      <Form.Label style={{ color: 'black' }}>Product Price</Form.Label>
                      <Form.Control
                        type="number"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                        required
                      />
                    </Form.Group>
                    <Button
                      variant="primary"
                      type="submit"
                      id="submitBtn"
                      className="mt-2">Update Product</Button>
                  </Form>
                </>
              ) : (
              <>
              <div className="image-container">
                <img className="sale-image" src={saleImage} alt="Sale" />
                <img className="sale-image" src={saleImage} alt="Sale" />
              </div>

                      <Card>
                      {imageUrl && <Card.Img variant="top" src={imageUrl} alt={name} />}

                        <Card.Body className="text-center">
                          <Card.Title>{productName}</Card.Title><br/>
                          <Card.Subtitle>Description:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                            {description}
                          </Card.Text>
                          <Card.Subtitle>Price:</Card.Subtitle>
                          <Card.Text style={{ color: 'black' }}>
                            Php {price}
                          </Card.Text>
                          {user.id ? (
                            <>
                              <div id="checkout-form">
                                <div className="item">
                                  <label
                                    htmlFor="quantity-select"
                                    style={{ color: 'black' }}>Quantity:</label>
                                  <select
                                    id="quantity-select"
                                    className="quantity-input px-5"
                                    style={{ color: 'black' }}
                                    value={quantity}
                                    onChange={(e) =>
                                      setQuantity(e.target.value)}>
                                    <option value="1" style={{ color: 'black' }}>1</option>
                                    <option value="2" style={{ color: 'black' }}>2</option>
                                    <option value="3" style={{ color: 'black' }}>3</option>
                                    <option value="4" style={{ color: 'black' }}>4</option>
                                    <option value="5" style={{ color: 'black' }}>5</option>
                                 
                                  </select>
                                </div>
                              </div><br/>
                               <Button variant="primary" onClick={() => order(productId, quantity)}>Buy</Button>
                            </>
                          ) : (
                            <Link className="btn btn-danger btn-block" to="/login">Log in to purchase</Link>
                          )}
                        </Card.Body>
                      </Card>
                      </>

              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
