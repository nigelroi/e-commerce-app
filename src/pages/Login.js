import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link,Navigate,useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import logo2 from '../icons/logo2.png'

export default function Login() {


    const navigate=useNavigate();

    const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);
    const [loading, setLoading] = useState(true);

    
	function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email,
                password : password
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(typeof data.access !== "undefined"){

                localStorage.setItem('token', data.access);
                console.log(data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login successful!',
                  text: 'Welcome Back to Eme-zone!',

                })
            navigate("/")
            } else {

                Swal.fire({
                  icon: 'error',
                  title: 'Authentication failed',
                  text: 'Please check your login credentials and try again.'
                })
            }
        })

        setEmail('');
        setPassword('');


    }

    const retrieveUserDetails = (token) => {


        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers : {
                Authorization : `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            setUser({
                firstName:data.firstName,
                id: data._id,
                isAdmin : data.isAdmin
            })
        });
    };

	useEffect(() => {


        if(email !== '' && password !== ''){

            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
        

      
        (user.id) ?
            <Navigate to="/products" />
        :        
            
            <Form onSubmit={(e) => authenticate(e)}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <img src={logo2} alt="Logo" style={{ maxWidth: '100%', height: 'auto' }} />
            </div>

            <div className="border container-fluid justify-content-center m-auto bg-secondary col-lg-6 p-3 p-md-5 text-center">
            <h1 className="mx-auto mb-5">Login</h1>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
            			onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
            			onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                { 
                	isActive ? 
    	                <Button variant="primary" type="submit" id="submitBtn" className="mt-2">
    	                    Login
    	                </Button>
    	                : 
    	                <Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>
    	                    Login
    	                </Button>
                }
                                <br />
                <Form.Text style={{ color: 'white' }}>Don't have an account yet? <Link to="/register">Sign up</Link></Form.Text>
                </div>
            </Form>
         

    )

}

