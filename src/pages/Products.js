import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import saleImage from '../images/sale.jpg';
import loadingImage from '../images/loading.gif';
import '../App.css';

export default function getAllActiveProducts() {
  const [activeProducts, setActiveProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchName, setSearchName] = useState('');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setActiveProducts(
          data.map((product) => {
            return <ProductCard key={product._id} product={product} isAdminPage={false} />;
          })
        );
        setLoading(false); // Set loading state to false once the data is fetched
      });
  }, []);

  const handleSearch = (e) => {
    setSearchName(e.target.value);
  };

  const filteredProducts = activeProducts.filter((product) => {
    return product.props.product.name.toLowerCase().includes(searchName.toLowerCase());
  });

  return (
    <>
      <div className="search-box text-center">
        <input
          type="text"
          placeholder="Search by name"
          value={searchName}
          onChange={handleSearch}
          style={{ width: '40%' }}
        />
      </div>
      <div className="image-container">
        <img className="sale-image" src={saleImage} alt="Sale" />
        <img className="sale-image" src={saleImage} alt="Sale" />
      </div>
      {loading ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
          <img className="loading-image img-fluid" src={loadingImage} alt="Loading products..." />
        </div>
      ) : (
        filteredProducts // Display the filtered products once the data is fetched
      )}
    </>
  );
}
