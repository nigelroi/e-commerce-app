import {useContext} from 'react';
import { Container, Card, Form,Button } from 'react-bootstrap';

import {useEffect,useState} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Link,Navigate,useNavigate } from 'react-router-dom';
import OrderCard from '../components/OrderCard';
import Swal from 'sweetalert2';



export default function Profile() {
  const { user } = useContext(UserContext);
  const [userOrderedProduct, setUserOrderedProduct] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserOrderedProduct(data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, []);

  return (
    <Container className="mt-5">
      <Card>
        <Card.Body className="text-center">
          <Card.Title>
            {user.firstName} {user.lastName}
          </Card.Title>
          <br />
          <Card.Subtitle>Registered Email:</Card.Subtitle>
          <Card.Text style={{ color: 'black' }}>{user.email}</Card.Text>
          <Card.Subtitle>Eme Points:</Card.Subtitle>
          <Card.Text style={{ color: 'black' }}>{user.points}</Card.Text>
        </Card.Body>
      </Card>

      {/* Render OrderCard for each order */}
      {userOrderedProduct.map((order) => (
        <OrderCard key={order._id} orders={order} isAdminPage={false} />
      ))}
    </Container>
  );
}
