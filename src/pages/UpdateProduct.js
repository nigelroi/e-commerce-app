import {useContext} from 'react';
import { Form, Button } from 'react-bootstrap';

import {useEffect,useState} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import {UserProvider} from '../UserContext';
import { Link,Navigate,useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function UpdateProduct() {
const {user} = useContext(UserContext);
const [productName, setProductName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState(0);

	const [user3, setUser3] = useState({ id : null, isAdmin : null });
function updateProduct(e) {
  // e.preventDefault();

      	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      	  method: "POST",
      	  headers: {
      	    "Content-Type": "application/json",
      	    Authorization : `Bearer ${ localStorage.getItem('token') }`
      	  },
      	  body: JSON.stringify({
      	  	name: productName,
      	  	description: description,
      	    price: price,
      	  })
      	})
        setProductName('');
        setDescription('');
        setPrice('');

        Swal.fire({
          icon: 'success',
          title: 'Product successfully updated the product.',
          text: 'You have successfully updated the product.'
        })

}

return(
		<>
		<br /><br /><br /><br />
		<Form onSubmit={(e) => updateProduct(e)}>
				<Form.Group controlId="productName">
					<Form.Label>Product Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the product's name"
						value={ productName }
						onChange={e => setProductName(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter product description"
						value={ description }
						onChange={e => setDescription(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Product Price</Form.Label>
					<Form.Control
						type="number"

						value={ price }
						onChange={e => setPrice(e.target.value)}
						required
					 />
				</Form.Group>
				<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Update Product</Button>
		</Form>
		</>
		)

}