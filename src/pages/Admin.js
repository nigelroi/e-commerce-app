import { useContext, useState, useEffect } from 'react';
import { Container, Card, Form, Button } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext, { UserProvider } from '../UserContext';
import ProductCard from '../components/ProductCard';
import OrderCard from '../components/OrderCard';
import AllOrdersCard from '../components/AllOrdersCard';




export default function getAllProducts(){
const { user } = useContext(UserContext);
const [userId, setUserId] = useState('');
const [userFirstName, setUserFirstName] = useState('');
const [userLastName, setUserLastName] = useState('');
const [userEmail, setUserEmail] = useState('');
const [userPoints, setUserPoints] = useState(0);
const [isAdmin, setIsAdmin] = useState(false);
const [userOrderedProduct, setUserOrderedProduct] = useState([]);
const [user3, setUser3] = useState({ id: null, isAdmin: null });
const [allProducts, setAllProducts] = useState([]);
const [isFound, setIsFound] = useState(false);
const navigate = useNavigate()
const [productName, setProductName] = useState('');
const [description, setDescription] = useState('');
const [price, setPrice] = useState(0);
const [isProducts, setIsProducts] = useState(true);
const [isUsers, setIsUsers] = useState(false);
const [isOrders, setIsOrders] = useState(false);
const [allOrders, setAllOrders] = useState([]);

	const [user2, setUser2] = useState({ id : null, isAdmin : null });

	function createProduct(e) {
  e.preventDefault();

      	fetch(`${process.env.REACT_APP_API_URL}/products`, {
      	  method: "POST",
      	  headers: {
      	    "Content-Type": "application/json",
      	    Authorization : `Bearer ${ localStorage.getItem('token') }`
      	  },
      	  body: JSON.stringify({
      	  	name: productName,
      	  	description: description,
      	    price: price,
      	  })
      	})
        setProductName('');
        setDescription('');
        setPrice('');

        Swal.fire({
          icon: 'success',
          title: 'Product successfully Registered!',
          text: 'You have successfully registered a product.'
        })

}



	useEffect(()=>{

		if(localStorage.getItem('token')){
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        })
		.then(res=>res.json())
		.then(data=>{

			console.log(data)
			setUser2({
			    id: data._id,
			    isAdmin: data.isAdmin
			})
			setAllProducts(data.map(product=>{

		return(
			<ProductCard key={product._id} product={product} isAdminPage={true}/>
			)
	}))
		})
	}
	},[])



  function setAdmin(userId) {
    // e.preventDefault();

          fetch(`${process.env.REACT_APP_API_URL}/users/user/setAdmin`, {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
      body: JSON.stringify({
        id: userId,
          }),
          })

          Swal.fire({
            icon: 'success',
            title: 'User is now an Admin.',
            text: 'You have successfully set user as Admin.'
          })

  }


  function getUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/user`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        id: userId,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          console.log(data);
          setUserFirstName(data.firstName);
          setUserLastName(data.lastName);
          setUserEmail(data.email);
          setUserPoints(data.points);
          setIsAdmin(data.isAdmin);
          setUserOrderedProduct(data.orderedProducts);
          setIsFound(true);
        } else {
          setIsFound(false);
        }
      })
      .catch((error) => {
        setIsFound(false);
        console.error('Error:', error);
      });
  }

  function getOrders() {
  // e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/products/userOrders`, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    }
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      setAllOrders(
        data.map((allOrders) => {
        	console.log(allOrders)
          return (
            <AllOrdersCard key={allOrders.productId} allOrders={allOrders} isOrders={true} />
          );
        })
      );
    })
    .catch((error) => {
      setIsFound(false);
      console.error('Error:', error);
    });
}

	return(
		<>
		<Card>
		<Card.Body>
		<h2>Create a Product</h2><br/>
		<Form onSubmit={(e) => createProduct(e)}>
				<Form.Group controlId="productName">
					<Form.Label style={{ color: 'black' }}>Product Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter the product's name"
						value={ productName }
						onChange={e => setProductName(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label style={{ color: 'black' }}>Description</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter product description"
						value={ description }
						onChange={e => setDescription(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label style={{ color: 'black' }}>Product Price</Form.Label>
					<Form.Control
						type="number"

						value={ price }
						onChange={e => setPrice(e.target.value)}
						required
					 />
				</Form.Group>
				<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Create Product</Button>
		</Form>
		</Card.Body>
		</Card>
		<br />
		
		   
		   <br />
		   <div className="d-flex flex-xs-column align-items-center col-s-2 col-lg-5 mx-auto text-center">
		   <Button className="mx-auto" variant="success" onClick={() => { setIsUsers(true); setIsProducts(false); setIsOrders(false)}}>Find/change User Info</Button>
		    <Button className="mx-auto" variant="success" onClick={() => {setIsProducts(true);setIsOrders(false);setIsUsers(false)}}>View all products</Button>
		    <Button className="mx-auto" variant="success" onClick={() =>{ setIsOrders(true); setIsProducts(false);setIsUsers(false);getOrders();}}>View all Orders</Button>
		   </div>
<br />

{isUsers && (
  <>
      <Card style={{ backgroundColor: 'darkgrey' }} className="my-3">
      <Card.Body>
    <Form onSubmit={(e) => getUser(e)}>
      <Form.Group controlId="userId">
        <Form.Label style={{ color: 'black' }}><h5>Find User by UserId</h5></Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter User ID"
          value={userId}
          onChange={(e) => setUserId(e.target.value)}
          required
        />
      </Form.Group>
      <Button variant="primary" type="submit" id="submitBtn" className="mt-2">Find User</Button>
    </Form>
      </Card.Body>
    </Card>

    {isFound && (
      <Container className="mt-5">
    <Card style={{ backgroundColor: 'darkgrey' }} className="my-3">
      <Card.Body>
            <Card.Title>{userFirstName} {userLastName}</Card.Title><br/>
            <Card.Subtitle>Registered Email:</Card.Subtitle>
            <Card.Text style={{ color: 'black' }}>{userEmail}</Card.Text>
            <Card.Subtitle>Eme Points:</Card.Subtitle>
            <Card.Text style={{ color: 'black' }}>{userPoints}</Card.Text>
            <Card.Subtitle>Is Admin:</Card.Subtitle>
            <Card.Text style={{ color: 'black' }}>{isAdmin !== null ? isAdmin.toString() : ''}</Card.Text>
            {isAdmin ? (
              <Button variant="success" disabled className="mt-2 float-right" >Set as Admin</Button>
            ) : (
              <Button variant="success" onClick={() => setAdmin(userId)} id="submitBtn2" className="mt-2 float-right">Set as Admin</Button>
            )}
          </Card.Body>
        </Card>
      </Container>
    )}
  </>
)}



{isOrders && allOrders}

{isProducts && allProducts}
		</>
		)


}