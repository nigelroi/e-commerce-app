import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link,Navigate,useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import logo2 from '../icons/logo2.png'

export default function Register() {

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const navigate=useNavigate();
	const [users,setUsers]=useState([])
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfuly binded
    // console.log(email);
    // console.log(password1);
    // console.log(password2);

    // Function to simulate user registration

// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
function registerUser(e) {
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email
    })
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data) {
        Swal.fire({
          icon: 'error',
          title: 'Email already exists',
          text: 'Email is already registered'
        });
      } else {

      	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      	  method: "POST",
      	  headers: {
      	    "Content-Type": "application/json",
      	  },
      	  body: JSON.stringify({
      	  	firstName: firstName,
      	  	lastName: lastName,
      	    email: email,
      	    password: password1
      	  })
      	})
        // Clear input fields
        setFirstName('');
        setLastName('');
        setEmail('');
        setPassword1('');
        setPassword2('');

        Swal.fire({
          icon: 'success',
          title: 'Successful Registered!',
          text: 'You have successfully registered an account.'
        });

        navigate("/login");
      }
    })
    .catch(error => {
      console.log(error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error occurred during registration. Please try again later.'
      });
    });
}







    useEffect(() => {
    	// Validation to enable submit button when all fields are populated and both passwords match
    	if((email !== "" && password1 !== "" && password2 !== ""&&firstName!==""&&lastName!=="") && (password1 === password2)){
    		setIsActive(true);
    	} else {
    		setIsActive(false);
    	}
    }, [email, password1, password2,firstName,lastName]);

	return(
		(user.id) ?
		    <Navigate to="/" />
		:
			<Form onSubmit={(e) => registerUser(e)}>
			<div style={{ display: 'flex', justifyContent: 'center' }}>
			  <img src={logo2} alt="Logo" style={{ maxWidth: '100%', height: 'auto' }} /><br/>
			  
			</div>
			<div className="border container-fluid justify-content-center m-auto bg-secondary col-lg-6 p-3 p-md-5 text-center">
			<h1>Register an account</h1>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your first name"
						value={ firstName }
						onChange={e => setFirstName(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your last name"
						value={ lastName }
						onChange={e => setLastName(e.target.value)}
						required
					 />
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter an email"
						value={ email }
						onChange={e => setEmail(e.target.value)}
						required
					 />
					 <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter a password"
						value={ password1 }
						onChange={e => setPassword1(e.target.value)}
						required
					 />
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Confirm your password"
						value={ password2 }
						onChange={e => setPassword2(e.target.value)}
						required
					 />
				</Form.Group>
				
				{/*conditional rendering for submit button based on isActive state*/}
				{
					isActive ?
						<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
						:
						<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
				}
				<br />
				<Form.Text style={{ color: 'white' }}>Already have an account? <Link to="/login">Sign in</Link></Form.Text>
				</div>
			</Form>

	)


}

